#include "mainwindow.h"
#include "logview.h"
#include "vectorlinestorage.h"
#include <QScrollArea>

MainWindow::MainWindow(QVector<QString>&& lines, QWidget *parent)
    : QMainWindow(parent)
{
    auto scroll = new QScrollArea(this);
    auto storage =
        std::make_unique<VectorLineStorage>(std::move(lines));
    scroll->setWidget(new LogView(std::move(storage), scroll));
    setCentralWidget(scroll);
}

MainWindow::~MainWindow()
{

}
