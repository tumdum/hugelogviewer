#include "logview.h"
#include "vectorlinestorage.h"
#include <QDebug>
#include <QPaintEvent>
#include <QPainter>
#include <QFontDatabase>
#include <QFontMetrics>

namespace
{
const int FONT_SIZE = 15;

QFont getFont()
{
    auto font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    font.setPointSize(FONT_SIZE);
    return font;
}


}

LogView::LogView(LineStoragePtr storage, QWidget *parent)
    : QWidget(parent)
    , storage_(std::move(storage))
{

}

QSize LogView::minimumSizeHint() const
{
    auto font = getFont();
    QFontMetrics metrics{font};

    const auto w =
        storage_->longestLine() * metrics.width('i');
    return QSize(w, metrics.lineSpacing()*(storage_->size()+1));
}

void LogView::paintEvent(QPaintEvent *event)
{
    auto font = getFont();
    QFontMetrics metrics{font};

    const auto lineHeight = metrics.lineSpacing();
    const auto start = event->rect().top() / lineHeight;

    auto end = event->rect().bottom() / lineHeight;

    if ((size_t)end == storage_->size())
    {
        --end;
    }

    qDebug() << "from" << start << "to" << end << "total" << end - start + 1;

    QPainter painter{this};
    painter.setFont(font);
    for (auto i = start; i <= end; ++i)
    {
        painter.drawText(0,(i+1)*metrics.lineSpacing(), storage_->at(i));
    }

}
