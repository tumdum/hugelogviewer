#-------------------------------------------------
#
# Project created by QtCreator 2017-04-01T10:38:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = loganalyzer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    logview.cpp \
    vectorlinestorage.cpp

HEADERS  += mainwindow.h \
    logview.h \
    linestorage.h \
    vectorlinestorage.h
