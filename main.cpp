#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Q_ASSERT(argc == 2);

    QFile input{argv[1]};
    QVector<QString> lines;
    Q_ASSERT(input.open(QIODevice::ReadOnly));

    QTextStream stream{&input};
    while (!stream.atEnd())
    {
        const auto line = stream.readLine();
        lines.push_back(line);
    }


    MainWindow w{std::move(lines)};
    w.show();

    return a.exec();
}
