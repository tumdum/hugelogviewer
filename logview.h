#ifndef LOGVIEW_H
#define LOGVIEW_H

#include <QWidget>
#include "linestorage.h"

class LogView : public QWidget
{
    Q_OBJECT
public:
    LogView(LineStoragePtr storage, QWidget *parent = 0);

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override { return minimumSizeHint(); }

signals:

public slots:

protected:
    void paintEvent(QPaintEvent* event) override;

private:
    LineStoragePtr storage_;
};

#endif // LOGVIEW_H
