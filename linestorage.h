#ifndef LINESTORAGE_H
#define LINESTORAGE_H

#include <cstddef>
#include <memory>
#include <QString>

class LineStorage
{
public:
    virtual ~LineStorage() {}

    // size returns number of lines in storage
    virtual size_t size() const = 0;

    // longestList returns number of characters in longest stored line
    virtual size_t longestLine() const = 0;

    // at returns n-th line from storage. When n is out of range
    // it throws.
    virtual QString at(size_t n) const = 0;
};

using LineStoragePtr = std::unique_ptr<LineStorage>;

#endif // LINESTORAGE_H
