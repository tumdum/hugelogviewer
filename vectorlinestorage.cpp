#include "vectorlinestorage.h"
#include <algorithm>

VectorLineStorage::VectorLineStorage(QVector<QString>&& lines)
    : lines_(std::move(lines))
{

}

size_t VectorLineStorage::size() const
{
    return lines_.size();
}

size_t VectorLineStorage::longestLine() const
{
    size_t m = 0;
    for (const auto& line : lines_)
    {
        m = std::max(m, (size_t)line.length());
    }
    return m;
}

QString VectorLineStorage::at(size_t n) const
{
    return lines_.at(n);
}
