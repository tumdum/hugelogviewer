#ifndef VECTORLINESTORAGE_H
#define VECTORLINESTORAGE_H

#include "linestorage.h"
#include <QVector>

class VectorLineStorage : public LineStorage
{
public:
    VectorLineStorage(QVector<QString>&& lines);

    size_t size() const override;

    size_t longestLine() const override;

    QString at(size_t n) const override;

private:
    QVector<QString> lines_;
};

#endif // VECTORLINESTORAGE_H
